package domain.models;

public class Hero {
    private long id;
    private String name;
    private String photo;
    private String description;
    private String role;
    private String difficulty;

    public Hero() {
    }

    public Hero(long id, String name, String photo, String description, String role, String difficulty) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.description = description;
        this.role = role;
        this.difficulty = difficulty;
    }

    public Hero(long id, String name, String photo, String description) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public String toString() {
        return "Hero{" +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", history='" + description + '\'' +
                ", f1='" + role + '\'' +
                ", f2='" + difficulty + '\'' +
                '}';
    }
}
