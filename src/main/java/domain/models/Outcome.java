package domain.models;

public class Outcome {
    private long id;
    private Item item;
    private int date;

    public Outcome() {
    }

    public Outcome(long id, Item item, int date) {
        this.id = id;
        this.item = item;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Spend money to:"+ item + ", date:" + date + " ";
    }
}
