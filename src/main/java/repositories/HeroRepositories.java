package repositories;

import domain.models.Hero;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IHeroRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class HeroRepositories implements IHeroRepository {
    private IDBRepository dbrepo = new PostqresRepositories();

    @Override
    public void add(Hero entity) {
        try {
            String sql = "INSERT INTO hero(name, photo, description, role, difficulty) " +
                    "VALUES(?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getPhoto());
            stmt.setString(3, entity.getDescription());
            stmt.setString(4, entity.getRole());
            stmt.setString(5, entity.getDifficulty());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Hero entity) {
        String sql = "UPDATE hero " +
                "SET ";
        int c = 0;
        if (entity.getPhoto() != null) {
            sql += "photo=?, ";
            c++;
        }
        if (entity.getDescription() != null) {
            sql += "description=?, ";
            c++;
        }
        if (entity.getRole() != null) {
            sql += "role=?, ";
            c++;
        }
        if (entity.getDifficulty() != null) {
            sql += "difficulty=?, ";
            c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE name = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getName() != null) {
                stmt.setString(i++, entity.getName());
            }
            if (entity.getPhoto() != null) {
                stmt.setString(i++, entity.getPhoto());
            }
            if (entity.getDescription() != null) {
                stmt.setString(i++, entity.getDescription());
            }
            if (entity.getRole() != null) {
                stmt.setString(i++, entity.getRole());
            }
            if (entity.getDifficulty() != null) {
                stmt.setString(i++, entity.getDifficulty());
            }
            stmt.setString(i++, entity.getName());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void remove(Hero entity) {

    }

    @Override
    public List<Hero> query(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Hero> heroes = new LinkedList();
            while (rs.next()) {
                Hero hero = new Hero(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("photo"),
                        rs.getString("description"),
                        //rs.getString("password"),
                        rs.getString("role"),
                        rs.getString("difficulty")
                );
                heroes.add(hero);
            }
            return heroes;
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
        }
    }

    @Override
    public Hero queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Hero(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("photo"),
                        rs.getString("description"),
                        rs.getString("role"),
                        rs.getString("difficulty")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public Hero getHeroByID(long id) {
        String sql = "SELECT * FROM hero WHERE id = " + id + " LIMIT 1";
        return queryOne(sql);
    }

    @Override
    public Hero getHeroByName(String name) {
        try {
            String sql = "SELECT * FROM hero WHERE name = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new Hero(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("photo"),
                        rs.getString("description"),
                        rs.getString("role"),
                        rs.getString("difficulty")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }
}

