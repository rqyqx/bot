package repositories.interfaces;

import domain.models.Hero;

public interface IHeroRepository extends IEntityRepository<Hero> {
    Hero getHeroByID(long id);

    //User findUserByLogin(UserLoginData data);

    Hero getHeroByName(String name);
}
