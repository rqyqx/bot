package services.interfaces;

import domain.models.Hero;

public interface IHeroService {
    Hero getHeroByID(long id);

    Hero getHeroByName(String name);

    void addHero(Hero hero);

    void updateHero(Hero hero);
}
