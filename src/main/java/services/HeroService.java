package services;

import domain.models.Hero;
import repositories.HeroRepositories;
import repositories.interfaces.IHeroRepository;
import services.interfaces.IHeroService;

public class HeroService implements IHeroService {
    private IHeroRepository heroRepo = new HeroRepositories();

    public Hero getHeroByID(long id) {
        return heroRepo.getHeroByID(id);
    }

    public Hero getHeroByName(String name) {
        return heroRepo.getHeroByName(name);
    }

    public void addHero(Hero hero) {
        heroRepo.add(hero);
    }

    @Override
    public void updateHero(Hero hero) {
        heroRepo.update(hero);
    }
}
