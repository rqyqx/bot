package controllers;

import domain.models.Hero;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import services.HeroService;
import services.interfaces.IHeroService;


import java.util.ArrayList;
import java.util.List;

public class Mybot extends TelegramLongPollingBot {
    private final IHeroService heroService = new HeroService();
    public static final String Token = "1268154266:AAEKpQplyBwpj2qbYxVconmHyGY6aYaCq7U";
    public static final String Username = "sonikdotabot";

    public void onUpdateReceived(Update update) {
        /*if(update.hasMessage()) {
            Message receivedMessage = update.getMessage();
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(receivedMessage.getChatId());

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> table = new ArrayList();

            List<InlineKeyboardButton> buttonRaw = new ArrayList();
            InlineKeyboardButton btn = new InlineKeyboardButton();

            btn.setText("Hero").setCallbackData("Hello, dear " +
                    receivedMessage.getFrom().getFirstName());
            buttonRaw.add(btn);


            table.add(buttonRaw);


            Hero hero = heroService.getHeroByID(1);
            buttonRaw = new ArrayList();
            btn = new InlineKeyboardButton();
            btn.setText("Author").setCallbackData(hero.toString());
            buttonRaw.add(btn);

            btn = new InlineKeyboardButton();
            btn.setText("Goodbye").setCallbackData("Oh, no, please, don`t go!");
            buttonRaw.add(btn);

            table.add(buttonRaw);


            inlineKeyboardMarkup.setKeyboard(table);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            sendMessage.setText("This is test bot...\n\n");
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        } else if (update.hasCallbackQuery()) {
            try {
                execute(new SendMessage().setText(
                        update.getCallbackQuery().getData())
                        .setChatId(update.getCallbackQuery().getMessage().getChatId()));
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        }
    }
*/

        if(update.hasMessage()) {
            Message receivedMessage = update.getMessage();
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(receivedMessage.getChatId());

            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> table = new ArrayList<>();

            List<InlineKeyboardButton> buttonRaw = new ArrayList<>();
            InlineKeyboardButton btn = new InlineKeyboardButton();

            btn.setText("Hello").setCallbackData("Hello, dear " +
                    receivedMessage.getFrom().getFirstName());
            buttonRaw.add(btn);


            table.add(buttonRaw);

            Hero hero = heroService.getHeroByID(1);
            buttonRaw = new ArrayList<>();
            btn = new InlineKeyboardButton();
            btn.setText("Author").setCallbackData(hero.toString());
            buttonRaw.add(btn);

            btn = new InlineKeyboardButton();
            btn.setText("Goodbye").setCallbackData("Oh, no, please, don`t go!");
            buttonRaw.add(btn);

            table.add(buttonRaw);


            inlineKeyboardMarkup.setKeyboard(table);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            sendMessage.setText("This is test bot...\n\n");
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        } else if (update.hasCallbackQuery()) {
            try {
                execute(new SendMessage().setText(
                        update.getCallbackQuery().getData())
                        .setChatId(update.getCallbackQuery().getMessage().getChatId()));
            } catch (TelegramApiException e) {
                System.out.println(e.getMessage());
            }
        }
    }



    public String getBotUsername() {
        return Username;
    }

    public String getBotToken() {
        return Token;
    }
}
